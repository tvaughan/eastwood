(ns eastwood.fulcro
  (:require
    [com.fulcrologic.fulcro.components :refer [defsc factory]]
    [com.fulcrologic.fulcro.routing.dynamic-routing :refer [defrouter]]))

(defsc HelloWorld
  [_ _]
  {:ident (fn [] [:component/id :hello-world])
   :initial-state {}
   :route-segment ["hello-world"]}
  "Hello, World!")

(defrouter SiteRouter
  [_ _]
  {:router-targets [HelloWorld]})
